const fs = require('fs');
const glob = require('glob');
const program = require('commander');

program
  .version('1.0.0')
  .option('-p, --pattern <pattern>', 'Glob pattern to find files')
  .on('--help', () => {
    console.log('  Examples:');
    console.log('');
    console.log('    $ replace-color-cli -p "./src/**/*.scss"');
    console.log('');
  });

program.parse(process.argv);

const colors = {
  primary: 'blue',
  neutral: 'gray',
  error: 'red',
  success: 'green',
};

const replaceInFile = (filePath) => {
  fs.readFile(filePath, 'utf8', (err, data) => {
    if (err) {
      return console.log(err);
    }

    let result = data;
    Object.keys(colors).forEach((color) => {
      result = result.replace(
        new RegExp(`\\$color-${color}-(\\d+)`, 'g'),
        `theme('colors.${colors[color]}.$1')`,
      );
    });

    fs.writeFile(filePath, result, 'utf8', (err) => {
      if (err) return console.log(err);
    });
  });
};

if (!program.pattern) {
  console.error(
    'Error: no pattern provided. Use -p or --pattern option to specify a pattern.',
  );
  process.exit(1);
}

glob(program.pattern, (err, files) => {
  if (err) {
    console.error(`Error: Invalid pattern provided: ${err.message}`);
    process.exit(1);
  } else {
    files.forEach(replaceInFile);
  }
});
