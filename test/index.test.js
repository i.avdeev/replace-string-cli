const assert = require('assert');
const fs = require('fs-extra');
const path = require('path');
const { exec, spawn } = require('child_process');

describe('replaceColorCli', () => {
  before(() => {
    fs.outputFileSync(
      path.join(__dirname, 'test-file-1.scss'),
      '$color-primary-800',
    );
    fs.outputFileSync(
      path.join(__dirname, 'test-file-2.scss'),
      '$color-neutral-700 $color-error-500',
    );
    fs.outputFileSync(
      path.join(__dirname, 'test-file-3.scss'),
      '$color-success-600 $color-primary-800',
    );
    fs.outputFileSync(
      path.join(__dirname, 'real-code.scss'),
      `
        .button {
            color: $color-primary-800;
            background-color: $color-neutral-700;
        }
    `,
    );
  });

  after(() => {
    fs.removeSync(path.join(__dirname, 'test-file-1.scss'));
    fs.removeSync(path.join(__dirname, 'test-file-2.scss'));
    fs.removeSync(path.join(__dirname, 'test-file-3.scss'));
    fs.removeSync(path.join(__dirname, 'real-code.scss'));
  });

  it("should replace $color-primary-800 to theme('colors.blue.800')", (done) => {
    exec(
      `node index.js -p ${path.join(__dirname, 'test-file-1.scss')}`,
      (err, stdout, stderr) => {
        if (err) {
          done(err);
          return;
        }
        assert.equal(
          fs.readFileSync(path.join(__dirname, 'test-file-1.scss'), 'utf8'),
          "theme('colors.blue.800')",
        );
        done();
      },
    );
  });

  it('should replace different colors and color values', (done) => {
    exec(
      `node index.js -p ${path.join(__dirname, 'test-file-2.scss')}`,
      (err, stdout, stderr) => {
        if (err) {
          done(err);
          return;
        }
        assert.equal(
          fs.readFileSync(path.join(__dirname, 'test-file-2.scss'), 'utf8'),
          "theme('colors.gray.700') theme('colors.red.500')",
        );
        done();
      },
    );
  });

  it('should replace multiple strings in SCSS file', (done) => {
    exec(
      `node index.js -p ${path.join(__dirname, 'test-file-3.scss')}`,
      (err, stdout, stderr) => {
        if (err) {
          done(err);
          return;
        }
        assert.equal(
          fs.readFileSync(path.join(__dirname, 'test-file-3.scss'), 'utf8'),
          "theme('colors.green.600') theme('colors.blue.800')",
        );
        done();
      },
    );
  });

  it('should work with multiple SCSS files', (done) => {
    exec(
      `node index.js -p ${path.join(__dirname, 'test-file-*')}`,
      (err, stdout, stderr) => {
        if (err) {
          done(err);
          return;
        }
        assert.equal(
          fs.readFileSync(path.join(__dirname, 'test-file-1.scss'), 'utf8'),
          "theme('colors.blue.800')",
        );
        assert.equal(
          fs.readFileSync(path.join(__dirname, 'test-file-2.scss'), 'utf8'),
          "theme('colors.gray.700') theme('colors.red.500')",
        );
        assert.equal(
          fs.readFileSync(path.join(__dirname, 'test-file-3.scss'), 'utf8'),
          "theme('colors.green.600') theme('colors.blue.800')",
        );
        done();
      },
    );
  });

  it('should work with real SCSS code', (done) => {
    exec(
      `node index.js -p ${path.join(__dirname, 'real-code.scss')}`,
      (err, stdout, stderr) => {
        if (err) {
          done(err);
          return;
        }
        assert.equal(
          fs.readFileSync(path.join(__dirname, 'real-code.scss'), 'utf8'),
          `
        .button {
            color: theme('colors.blue.800');
            background-color: theme('colors.gray.700');
        }
    `,
        );
        fs.removeSync(path.join(__dirname, 'real-code.scss'));
        done();
      },
    );
  });

  it('should exit with error code when pattern option is missing', (done) => {
    const child = spawn('node', ['index.js']);
    child.on('exit', (code) => {
      assert.strictEqual(code, 1);
      done();
    });
  });
});
